package com.higher.construshow;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.higher.construshow.db.VendedorDao;
import com.higher.construshow.view.EmpresasActivity;
import com.higher.construshow.view.LoginActivity;
import com.higher.construshow.view.PrincipalActivity;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mostrarLogin();
            }
        }, 1000);
    }

    private void mostrarLogin() {
        VendedorDao vendedorDao = new VendedorDao(MainActivity.this);

        if(vendedorDao.retorna() != null){
            startActivity(new Intent(MainActivity.this, EmpresasActivity.class));
            finish();
        }else{
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }

    }
}
