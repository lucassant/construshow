package com.higher.construshow.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import androidx.appcompat.app.AlertDialog;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.higher.construshow.R;
import com.higher.construshow.db.DepartamentoDao;
import com.higher.construshow.db.EmpresaDao;
import com.higher.construshow.db.ProdutoDao;
import com.higher.construshow.model.Departamento;
import com.higher.construshow.model.Empresa;
import com.higher.construshow.model.Produto;
import com.higher.construshow.model.ProdutoPesoImposto;
import com.higher.construshow.view.ImportarActivity;

import org.json.JSONException;

import java.util.List;

public class Sincronizacao {

    private Empresa empresa = null;
    private Context context;
    private AlertDialog dialogSincronizacao;

    public Sincronizacao(Context context) {
        EmpresaDao empresaDao = new EmpresaDao(context);
        empresa = empresaDao.carrega();
        this.context = context;
    }

    public void sincronizaProdutos() {
        if (temConexaoInternet()) {
            if (empresa != null) {
                //Só sincroniza se n for a primeira vez
                if (!empresa.getUltimaSinc().equals("2010-01-01")) {

                    exibeDialogSincronizar();

                    String url = Repositorio.IP_CONEXAO_2 + "produtos/" + empresa.getUltimaSinc2();

                    RequestQueue requestQueue = Volley.newRequestQueue(context);
                    final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, response -> {
                        Gson gson = new Gson();

                        try {
                            List<ProdutoPesoImposto> lista = gson.fromJson(response.getJSONArray("produtos").toString(), new TypeToken<List<ProdutoPesoImposto>>() {
                            }.getType());

                            ProdutoDao produtoDao = new ProdutoDao(context);
                            for (ProdutoPesoImposto model : lista) {
                                produtoDao.atualizaDadosProduto(model);
                            }

                            sincronizaDepartamentos();

                        } catch (JSONException e) {
                            Log.d("exc produtos", e.toString());
                            dialogSincronizacao.dismiss();
                        }
                    }, error -> {
                        Log.d("error produtos", error.toString());
                        dialogSincronizacao.dismiss();
                    });

                    jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    requestQueue.add(jsonObjectRequest);
                }
            }
        }else{
            Log.d("conexao", "sem conexao internet");
        }

    }

    public void sincronizaDepartamentos() {
        if (empresa != null) {
            String url = Repositorio.IP_CONEXAO_2 + "departamentos";

            RequestQueue requestQueue = Volley.newRequestQueue(context);
            final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, response -> {
                Gson gson = new Gson();

                try {
                    List<Departamento> lista = gson.fromJson(response.getJSONArray("departamentos").toString(), new TypeToken<List<Departamento>>() {
                    }.getType());

                    DepartamentoDao dao = new DepartamentoDao(context);
                    for (Departamento model : lista) {
                        dao.atualiza(model);
                    }

                    //Atualiza data da sincronização
                    atualizaDataSinc();
                    Log.d("sincronizacao", "terminou os departamentos");
                    dialogSincronizacao.dismiss();

                } catch (JSONException e) {
                    Log.d("exc depto", e.toString());
                    dialogSincronizacao.dismiss();
                }
            }, error -> {
                Log.d("error depto", error.toString());
                dialogSincronizacao.dismiss();
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjectRequest);
        }
    }

    private void atualizaDataSinc() {
        EmpresaDao _empresaDao = new EmpresaDao(context);
        empresa.setUltimaSinc2(Repositorio.formataDataParaSQL(Repositorio.retornaDataAtual()));
        _empresaDao.atualiza(empresa);
    }

    private void exibeDialogSincronizar() {
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View view = layoutInflater.inflate(R.layout.dialog_sincronizacao, null);
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setView(view);
        alertBuilder.setCancelable(false);

        //Cria o dialog
        dialogSincronizacao = alertBuilder.create();
        dialogSincronizacao.show();
    }

    private boolean temConexaoInternet() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return (activeNetwork != null && activeNetwork.isConnected());
    }
}
