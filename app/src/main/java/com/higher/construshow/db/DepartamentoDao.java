package com.higher.construshow.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.higher.construshow.model.Departamento;

public class DepartamentoDao {

    private String TABLE_NAME = "departamento";
    private SQLiteDatabase le, escreve;
    private Database db;

    public DepartamentoDao(Context c) {
        db = Database.getInstance(c);
        le = db.getReadableDatabase();
        escreve = db.getWritableDatabase();
    }

    private long insere(Departamento model){
        ContentValues cv = new ContentValues();

        cv.put("id", model.getId());
        cv.put("nome", model.getNome());
        cv.put("desconto_maximo", model.getDescontoMaximo());
        cv.put("acrescimo_maximo", model.getAcrescimoMaximo());

        return escreve.insert(TABLE_NAME, null, cv);
    }

    private long update(Departamento model){
        ContentValues cv = new ContentValues();

        cv.put("desconto_maximo", model.getDescontoMaximo());
        cv.put("acrescimo_maximo", model.getAcrescimoMaximo());

        return escreve.update(TABLE_NAME, cv, "id = ?", new String[]{ String.valueOf(model.getId()) });
    }

    public void atualiza(Departamento model){
        String sql = "SELECT id FROM departamento WHERE id = ?";
        Cursor c = le.rawQuery(sql, new String[]{ String.valueOf(model.getId()) });
        c.moveToFirst();

        if(c.getCount() > 0){
            update(model);
        }else{
            insere(model);
        }
        c.close();
    }

    public Departamento carrega(int id){
        Departamento _model = new Departamento();

        String sql = "SELECT id, nome, desconto_maximo, acrescimo_maximo FROM departamento WHERE id = ?";
        Cursor c = le.rawQuery(sql, new String[]{ String.valueOf(id) });
        c.moveToFirst();

        if(c.getCount() > 0){
            _model.setId(c.getInt(0));
            _model.setNome(c.getString(1));
            _model.setDescontoMaximo(c.getDouble(2));
            _model.setAcrescimoMaximo(c.getDouble(3));
        }else{
            _model = null;
        }
        c.close();

        return  _model;
    }
}
