package com.higher.construshow.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.higher.construshow.model.Empresa;

public class EmpresaDao {

    private String TABLE_NAME = "empresa";
    private SQLiteDatabase le, escreve;
    private Database db;

    public EmpresaDao(Context c) {
        db = Database.getInstance(c);
        le = db.getReadableDatabase();
        escreve = db.getWritableDatabase();
    }

    private long insere(Empresa empresa) {
        ContentValues cv = new ContentValues();

        cv.put("id", empresa.getId());
        cv.put("nome", empresa.getNome());
        cv.put("ultima_sinc", "2010-01-01");
        cv.put("ultima_sinc_img", "2010-01-01");
        cv.put("ultima_sinc_2", "2010-01-01");

        return escreve.insert(TABLE_NAME, null, cv);
    }

    public long atualiza(Empresa empresa){
        ContentValues cv = new ContentValues();

        cv.put("ultima_sinc", empresa.getUltimaSinc());
        cv.put("ultima_sinc_img", empresa.getUltimaSincImg());
        cv.put("ultima_sinc_2", empresa.getUltimaSinc2());

        return escreve.update(TABLE_NAME, cv, "id = ?", new String[] { String.valueOf(empresa.getId()) });
    }

    public long salvar(Empresa empresa) {
        long retorno = 0;

        String sql = "SELECT id FROM empresa WHERE id = ?";

        Cursor c = le.rawQuery(sql, new String[]{ String.valueOf(empresa.getId()) });
        c.moveToFirst();

        if(c.getCount() == 0){
            retorno = insere(empresa);
        }
        c.close();

        return  retorno;
    }

    public Empresa carrega() {
        Empresa empresa = new Empresa();

        String sql = "SELECT id, nome, ultima_sinc, ultima_sinc_img, ultima_sinc_2 FROM empresa LIMIT 1";

        Cursor c = le.rawQuery(sql, null);
        c.moveToFirst();

        if (c.getCount() > 0) {
            empresa.setId(c.getInt(0));
            empresa.setNome(c.getString(1));
            empresa.setUltimaSinc(c.getString(2));
            empresa.setUltimaSincImg(c.getString(3));
            empresa.setUltimaSinc2(c.getString(4));
        } else {
            empresa = null;
        }

        c.close();

        return empresa;
    }
}
