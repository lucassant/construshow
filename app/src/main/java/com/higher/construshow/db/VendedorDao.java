package com.higher.construshow.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.higher.construshow.model.Vendedor;

public class VendedorDao {

    private String TABLE_NAME = "vendedor";
    private SQLiteDatabase le, escreve;
    private Database db;

    public VendedorDao(Context c) {
        db = Database.getInstance(c);
        le = db.getReadableDatabase();
        escreve = db.getWritableDatabase();
    }

    public long insere(Vendedor model){
        ContentValues cv = new ContentValues();

        cv.put("id", model.getId());
        cv.put("id_pessoa", model.getIdPessoa());
        cv.put("nome", model.getNome());
        cv.put("email", model.getEmail());
        cv.put("user_id", model.getUserId());

        return escreve.insert(TABLE_NAME, null, cv);
    }

    public long atualizaFlex(double novoValor){
        ContentValues cv = new ContentValues();

        cv.put("saldo_flex", novoValor);

        return escreve.update(TABLE_NAME, cv, null, null);
    }

    public Vendedor retorna(){
        Vendedor vendedor = new Vendedor();

        String sql = "SELECT " +
                        "id, " +
                        "id_pessoa, " +
                        "nome, " +
                        "email, " +
                        "user_id, " +
                        "saldo_flex " +
                    "FROM " +
                        "vendedor " +
                    "LIMIT 1";

        Cursor c = le.rawQuery(sql, null);
        c.moveToFirst();

        if(c.getCount() > 0){
            vendedor.setId(c.getInt(0));
            vendedor.setIdPessoa(c.getInt(1));
            vendedor.setNome(c.getString(2));
            vendedor.setEmail(c.getString(3));
            vendedor.setUserId(c.getString(4));
            vendedor.setSaldoFlex(c.getDouble(5));
        }else{
            vendedor = null;
        }
        c.close();

        return vendedor;
    }
}
