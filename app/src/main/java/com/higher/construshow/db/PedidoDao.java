package com.higher.construshow.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.higher.construshow.model.Pedido;
import com.higher.construshow.util.Repositorio;

import java.util.ArrayList;
import java.util.List;

public class PedidoDao {

    private String TABLE_NAME = "pedido";
    private SQLiteDatabase le, escreve;
    private Database db;

    public PedidoDao(Context c) {
        db = Database.getInstance(c);
        le = db.getReadableDatabase();
        escreve = db.getWritableDatabase();
    }

    public long insere(Pedido model) {
        ContentValues cv = new ContentValues();

        cv.put("id", model.getId());
        cv.put("id_estab", model.getIdEstab());
        cv.put("id_cliente", model.getIdCliente());
        cv.put("id_vendedor", model.getIdVendedor());
        cv.put("forma_pagamento", model.getFormaPagamento());
        cv.put("id_tab_preco", model.getIdTabelaPreco());
        cv.put("id_parcelamento", model.getIdParcelamento());
        cv.put("id_endereco", model.getIdEndereco());
        cv.put("data", model.getData());
        cv.put("qtd_volume", model.getQtdVolume());
        cv.put("observacao", model.getObservacao());
        cv.put("id_departamento", model.getIdDepartamento());

        return escreve.insert(TABLE_NAME, null, cv);
    }

    public long atualiza(Pedido model) {
        ContentValues cv = new ContentValues();

        cv.put("id_estab", model.getIdEstab());
        cv.put("forma_pagamento", model.getFormaPagamento());
        cv.put("id_tab_preco", model.getIdTabelaPreco());
        cv.put("id_parcelamento", model.getIdParcelamento());
        cv.put("data", model.getData());
        cv.put("qtd_volume", model.getQtdVolume());
        cv.put("observacao", model.getObservacao());
        cv.put("qtd_produtos", model.getQtdProdutos());
        cv.put("valor_total", model.getValorTotal());
        cv.put("valor_liquido", model.getValorLiquido());
        cv.put("enviado", model.getEnviado());

        return escreve.update(TABLE_NAME, cv, "id = ?", new String[]{String.valueOf(model.getId())});
    }

    public long save(Pedido model) {
        long retorno = 0;

        String sql = "SELECT id FROM pedido WHERE id = ?";

        Cursor c = le.rawQuery(sql, new String[]{String.valueOf(model.getId())});
        c.moveToFirst();

        if (c.getCount() > 0) {
            long _aux = atualiza(model);

            //Se ele atualizou então retorna um valor maior que 0
            if (_aux > 0) {
                retorno = c.getLong(0);
            }
        } else {
            retorno = insere(model);
        }
        c.close();

        return retorno;
    }

    public long atualizaParaEnviado(Pedido model) {
        ContentValues cv = new ContentValues();

        cv.put("id_carrinho", model.getIdCarrinho());
        cv.put("enviado", 1);

        return escreve.update(TABLE_NAME, cv, "id = ?", new String[]{String.valueOf(model.getId())});
    }

    public List<Pedido> lista(String tipo, String pesquisa) {
        List<Pedido> retorno = new ArrayList<Pedido>();

        String sql =
                "SELECT " +
                        "pe.id, " +
                        "pe.id_cliente," +
                        "pe.id_estab, " +
                        "pe.id_endereco, " +
                        "pe.id_vendedor, " +
                        "pe.data, " +
                        "pe.qtd_produtos, " +
                        "pe.valor_total, " +
                        "pe.qtd_volume, " +
                        "pe.observacao, " +
                        "cl.nome, " +
                        "en.endereco ," +
                        "pe.enviado, " +
                        "pe.id_carrinho, " +
                        "pe.forma_pagamento, " +
                        "pe.id_tab_preco, " +
                        "pe.id_parcelamento, " +
                        "tb.perc_padrao, " +
                        "tb.tipo, " +
                        "pe.valor_flex, " +
                        "pe.id_departamento, " +
                        "tb.descricao, " +
                        "pe.valor_liquido " +
                        "FROM " +
                        "pedido pe " +
                        "INNER JOIN cliente cl ON pe.id_cliente = cl.id " +
                        "INNER JOIN endereco en ON pe.id_endereco = en.id_endereco " +
                        "INNER JOIN tabela_preco tb ON pe.id_tab_preco = tb.id ";

        if (tipo.equals("2")) {
            sql += "WHERE " +
                    "(cl.nome LIKE ? OR cl.cnpjf LIKE ? OR cl.fantasia LIKE ?) ";
        } else {
            sql += "WHERE " +
                    "pe.enviado = ? AND (cl.nome LIKE ? OR cl.cnpjf LIKE ? OR cl.fantasia LIKE ?) ";
        }

        sql += "ORDER BY " +
                "pe.id DESC";

        Cursor c = le.rawQuery(sql, new String[]{tipo, "%" + pesquisa + "%"});
        c.moveToFirst();

        while (!c.isAfterLast()) {
            Pedido pe = new Pedido();

            pe.setId(c.getInt(0));
            pe.setIdCliente(c.getInt(1));
            pe.setIdEstab(c.getInt(2));
            pe.setIdEndereco(c.getInt(3));
            pe.setIdVendedor(c.getInt(4));
            pe.setData(c.getString(5));
            pe.setQtdProdutos(c.getInt(6));

            //Calcula o valor de acordo com a tabela de preço
            double _auxValor = valorTotalPedido(c.getString(0));

            if (c.getString(18).equals("A")) {
                _auxValor += _auxValor * (c.getDouble(17) / 100);
            } else {
                _auxValor -= _auxValor * (c.getDouble(17) / 100);
            }
            pe.setValorTotal(_auxValor);

            pe.setQtdVolume(c.getDouble(8));
            pe.setObservacao(c.getString(9));
            pe.setNomeCliente(c.getString(10));
            pe.setEndereco(c.getString(11));
            pe.setEnviado(c.getInt(12));
            pe.setIdCarrinho(c.getInt(13));
            pe.setFormaPagamento(c.getString(14));
            pe.setIdTabelaPreco(c.getInt(15));
            pe.setIdParcelamento(c.getInt(16));
            pe.setValorFlex(c.getDouble(19));
            pe.setIdDepartamento(c.getInt(20));
            pe.setNomeTabPreco(c.getString(21));
            pe.setValorLiquido(c.getDouble(22));

            retorno.add(pe);
            c.moveToNext();
        }

        c.close();

        return retorno;
    }

    public Pedido retorna(String numPedido) {
        Pedido pe = new Pedido();

        String sql =
                "SELECT " +
                        "pe.id, " +
                        "pe.id_cliente," +
                        "pe.id_estab, " +
                        "pe.id_endereco, " +
                        "pe.id_vendedor, " +
                        "pe.data, " +
                        "pe.qtd_produtos, " +
                        "pe.valor_total, " +
                        "pe.qtd_volume, " +
                        "pe.observacao, " +
                        "cl.nome, " +
                        "en.endereco ," +
                        "pe.enviado, " +
                        "pe.forma_pagamento, " +
                        "pe.id_tab_preco, " +
                        "pe.id_parcelamento, " +
                        "tb.perc_padrao, " +
                        "tb.tipo, " +
                        "pe.valor_flex, " +
                        "pe.id_departamento, " +
                        "ROUND(pe.valor_liquido, 2) " +
                        "FROM " +
                        "pedido pe " +
                        "INNER JOIN cliente cl ON pe.id_cliente = cl.id " +
                        "INNER JOIN endereco en ON pe.id_endereco = en.id_endereco " +
                        "INNER JOIN tabela_preco tb ON pe.id_tab_preco = tb.id " +
                        "WHERE " +
                        "pe.id = ? ";

        Cursor c = le.rawQuery(sql, new String[]{numPedido});
        c.moveToFirst();

        if (c.getCount() > 0) {

            pe.setId(c.getInt(0));
            pe.setIdCliente(c.getInt(1));
            pe.setIdEstab(c.getInt(2));
            pe.setIdEndereco(c.getInt(3));
            pe.setIdVendedor(c.getInt(4));
            pe.setData(Repositorio.dataBancoParaConstrushow(c.getString(5)));
            pe.setQtdProdutos(c.getInt(6));

            //Calcula o valor de acordo com a tabela de preço
            double _auxValor = valorTotalPedido(numPedido);
            if (c.getString(17).equals("A")) {
                _auxValor += _auxValor * (c.getDouble(16) / 100);
            } else {
                _auxValor -= _auxValor * (c.getDouble(16) / 100);
            }
            pe.setValorTotal(_auxValor);

            pe.setQtdVolume(c.getDouble(8));
            pe.setObservacao(c.getString(9));
            pe.setNomeCliente(c.getString(10));
            pe.setEndereco(c.getString(11));
            pe.setEnviado(c.getInt(12));
            pe.setFormaPagamento(c.getString(13));
            pe.setIdTabelaPreco(c.getInt(14));
            pe.setIdParcelamento(c.getInt(15));
            pe.setValorFlex(c.getDouble(18));
            pe.setIdDepartamento(c.getInt(19));
            pe.setValorLiquido(c.getDouble(20));

        } else {
            pe = null;
        }
        c.close();

        return pe;
    }

    public int nextNumPedido() {
        int retorno = 0;

        String sql = "SELECT MAX(id) FROM pedido";
        Cursor c = le.rawQuery(sql, null);
        c.moveToFirst();

        if (c.getCount() > 0) {
            retorno = c.getInt(0);
        }
        c.close();

        retorno += 1;

        return retorno;
    }

    public double valorTotalPedido(String idPedido) {
        double retorno = 0.00;

        String sql = "SELECT preco, desc_item, quantidade, porc_imposto, peso_unidade FROM pedido_lancto WHERE id_pedido = ?";
        Cursor c = le.rawQuery(sql, new String[]{idPedido});
        c.moveToFirst();

        while(!c.isAfterLast()){
            double _auxPreco = c.getDouble(0);

            //Abate o desconto
            _auxPreco = _auxPreco - (_auxPreco * (c.getDouble(1) / 100));

            //Multiplica pelo peso
            if(c.getDouble(4) > 0.00) _auxPreco = _auxPreco * c.getDouble(4);

            //Calcula o valor do imposto do produto (%)
            _auxPreco += _auxPreco * (c.getDouble(3) / 100);

            //Multiplica pela quantidade
            _auxPreco = _auxPreco * c.getDouble(2);

            retorno += _auxPreco;

            c.moveToNext();
        }
        c.close();

        return retorno;
    }

    public int qtdTotalProdutos(String idPedido) {
        int retorno = 0;

        String sql = "SELECT COUNT(sequencia) FROM pedido_lancto WHERE id_pedido = ?";

        Cursor c = le.rawQuery(sql, new String[]{idPedido});
        c.moveToFirst();

        if (c.getCount() > 0) {
            retorno = c.getInt(0);
        }
        c.close();

        return retorno;
    }

    public double retornaTotalFlex(String idPedido) {
        double retorno = 0.00;

        String sql = "SELECT valor_flex FROM pedido WHERE id = ?";
        Cursor c = le.rawQuery(sql, new String[]{idPedido});
        c.moveToFirst();

        if (c.getCount() > 0) {
            retorno = c.getDouble(0);
        }
        c.close();

        return retorno;
    }

    public void excluiPedido(String idPedido) {
        escreve.execSQL("DELETE FROM pedido_lancto WHERE id_pedido = " + idPedido);
        escreve.execSQL("DELETE FROM pedido WHERE id = " + idPedido);
    }
}
