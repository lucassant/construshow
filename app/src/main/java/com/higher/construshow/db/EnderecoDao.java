package com.higher.construshow.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.higher.construshow.model.Endereco;

import java.util.ArrayList;
import java.util.List;

public class EnderecoDao {

    private String TABLE_NAME = "endereco";
    private SQLiteDatabase le, escreve;
    private Database db;

    public EnderecoDao(Context c) {
        db = Database.getInstance(c);
        le = db.getReadableDatabase();
        escreve = db.getWritableDatabase();
    }

    public long insere(Endereco model) {
        ContentValues cv = new ContentValues();

        cv.put("id_cliente", model.getIdCliente());
        cv.put("id_endereco", model.getIdEndereco());
        cv.put("id_estab", model.getIdEstab());
        cv.put("tipo_end", model.getTipo());
        cv.put("endereco", model.getEndereco());
        cv.put("complemento", model.getComplemento());
        cv.put("numero", model.getNumero());
        cv.put("bairro", model.getBairro());
        cv.put("id_cidade", model.getIdCidade());
        cv.put("cidade", model.getCidade());
        cv.put("cep", model.getCep());
        cv.put("telefone", model.getTelefone());
        cv.put("uf", model.getUf());

        long id = escreve.insert(TABLE_NAME, null, cv);

        return id;
    }

    public long atualiza(Endereco model) {
        ContentValues cv = new ContentValues();

        cv.put("id_cliente", model.getIdCliente());
        cv.put("tipo_end", model.getTipo());
        cv.put("endereco", model.getEndereco());
        cv.put("complemento", model.getComplemento());
        cv.put("numero", model.getNumero());
        cv.put("bairro", model.getBairro());
        cv.put("id_cidade", model.getIdCidade());
        cv.put("cidade", model.getCidade());
        cv.put("cep", model.getCep());
        cv.put("telefone", model.getTelefone());
        cv.put("uf", model.getUf());

        long id = escreve.update(TABLE_NAME, cv, "id_estab = ? AND id_endereco = ?", new String[]{model.getIdEstab(), model.getIdEndereco()});

        return id;
    }

    public long insertOuUpdate(Endereco model) {

        //Verifica se o cliente já existe
        Cursor c = le.rawQuery("SELECT id_endereco FROM endereco WHERE id_endereco = ? AND id_estab = ?", new String[]{model.getIdEndereco(), model.getIdEstab()});
        c.moveToFirst();

        long id = 0;

        if (c != null) {
            if (c.getCount() > 0) {
                //Já existe
                id = atualiza(model);
            } else {
                //Não existe
                id = insere(model);
            }
        }
        c.close();

        return id;
    }

    public Endereco retorna(String idEndereco) {
        Endereco endereco = new Endereco();

        String sql =
                "SELECT " +
                        "id_cliente," +
                        "id_endereco," +
                        "id_estab," +
                        "tipo_end," +
                        "endereco," +
                        "COALESCE(complemento, '')," +
                        "COALESCE(numero, '')," +
                        "bairro," +
                        "cidade," +
                        "cep," +
                        "telefone, " +
                        "uf " +
                        "FROM " +
                        "endereco " +
                        "WHERE " +
                        "id_endereco = ? " +
                        "ORDER BY " +
                        "tipo_end DESC";

        Cursor c = le.rawQuery(sql, new String[]{idEndereco});
        c.moveToFirst();

        if (c.getCount() > 0) {

            endereco.setIdCliente(c.getString(0));
            endereco.setIdEndereco(c.getString(1));
            endereco.setIdEstab(c.getString(2));
            endereco.setTipo(c.getString(3));
            endereco.setEndereco(c.getString(4));
            endereco.setComplemento(c.getString(5));
            endereco.setNumero(c.getString(6));
            endereco.setBairro(c.getString(7));
            endereco.setCidade(c.getString(8));
            endereco.setCep(c.getString(9));
            endereco.setTelefone(c.getString(10));
            endereco.setUf(c.getString(11));

            c.moveToNext();
        }else{
            endereco = null;
        }
        c.close();

        return endereco;
    }

    public List<Endereco> lista(String idPess) {
        List<Endereco> retorno = new ArrayList<>();

        String sql =
                "SELECT " +
                        "id_cliente," +
                        "id_endereco," +
                        "id_estab," +
                        "tipo_end," +
                        "endereco," +
                        "COALESCE(complemento, '')," +
                        "COALESCE(numero, '')," +
                        "bairro," +
                        "cidade," +
                        "cep," +
                        "telefone, " +
                        "uf " +
                        "FROM " +
                        "endereco " +
                        "WHERE " +
                        "id_cliente = ? " +
                        "ORDER BY " +
                        "tipo_end DESC";

        Cursor c = le.rawQuery(sql, new String[]{idPess});
        c.moveToFirst();

        if (c != null) {
            while (!c.isAfterLast()) {
                Endereco endereco = new Endereco();

                endereco.setIdCliente(c.getString(0));
                endereco.setIdEndereco(c.getString(1));
                endereco.setIdEstab(c.getString(2));
                endereco.setTipo(c.getString(3));
                endereco.setEndereco(c.getString(4));
                endereco.setComplemento(c.getString(5));
                endereco.setNumero(c.getString(6));
                endereco.setBairro(c.getString(7));
                endereco.setCidade(c.getString(8));
                endereco.setCep(c.getString(9));
                endereco.setTelefone(c.getString(10));
                endereco.setUf(c.getString(11));

                retorno.add(endereco);

                c.moveToNext();
            }
        }
        c.close();

        return retorno;
    }
}
