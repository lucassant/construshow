package com.higher.construshow.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.higher.construshow.model.Cliente;
import com.higher.construshow.model.TabelaPreco;

import java.util.ArrayList;
import java.util.List;

public class TabelaPrecoDao {

    private String TABLE_NAME = "tabela_preco";
    private SQLiteDatabase le, escreve;
    private Database db;

    public TabelaPrecoDao(Context c) {
        db = Database.getInstance(c);
        le = db.getReadableDatabase();
        escreve = db.getWritableDatabase();
    }

    public long insere(TabelaPreco model) {
        ContentValues cv = new ContentValues();

        cv.put("id", model.getId());
        cv.put("id_estab", model.getIdEstab());
        cv.put("descricao", model.getDescricao());
        cv.put("tipo", model.getTipo());
        cv.put("perc_padrao", model.getPercentualPadrao());
        cv.put("permite_desconto", model.getPermiteDesconto());
        cv.put("prioriza_promocao", model.getPriorizaPromocao());
        cv.put("aplica_em", model.getAplicaEm());

        long id = escreve.insert(TABLE_NAME, null, cv);

        return id;
    }

    public long atualiza(TabelaPreco model) {
        ContentValues cv = new ContentValues();

        cv.put("descricao", model.getDescricao());
        cv.put("tipo", model.getTipo());
        cv.put("perc_padrao", model.getPercentualPadrao());
        cv.put("permite_desconto", model.getPermiteDesconto());
        cv.put("prioriza_promocao", model.getPriorizaPromocao());
        cv.put("aplica_em", model.getAplicaEm());

        long id = escreve.update(TABLE_NAME, cv, "id = ? AND id_estab = ?", new String[]{String.valueOf(model.getId()), String.valueOf(model.getIdEstab())});

        return id;
    }

    public long insertOuUpdate(TabelaPreco model) {

        //Verifica se o cliente já existe
        Cursor c = le.rawQuery("SELECT id FROM tabela_preco WHERE id = ? AND id_estab = ?", new String[]{String.valueOf(model.getId()), String.valueOf(model.getIdEstab())});
        c.moveToFirst();

        long id = 0;

        if (c != null) {
            if (c.getCount() > 0) {
                //Já existe
                id = atualiza(model);
            } else {
                //Não existe
                id = insere(model);
            }
        }
        c.close();

        return id;
    }


    public TabelaPreco retorna(int id) {
        TabelaPreco model = new TabelaPreco();

        String sql =
                "SELECT " +
                        "id, " +
                        "id_estab, " +
                        "descricao, " +
                        "tipo, " +
                        "perc_padrao," +
                        "permite_desconto, " +
                        "prioriza_promocao, " +
                        "aplica_em " +
                        "FROM " +
                        "tabela_preco " +
                        "WHERE " +
                        "id = ? " +
                        "ORDER BY " +
                        "descricao";

        Cursor c = le.rawQuery(sql, new String[]{String.valueOf(id)});
        c.moveToFirst();

        if(c.getCount() > 0){

            model.setId(c.getInt(0));
            model.setIdEstab(c.getInt(1));
            model.setDescricao(c.getString(2));
            model.setTipo(c.getString(3));
            model.setPercentualPadrao(c.getDouble(4));
            model.setPermiteDesconto(c.getString(5));
            model.setPriorizaPromocao(c.getString(6));
            model.setAplicaEm(c.getString(7));
        }else{
            model = null;
        }
        c.close();

        return model;
    }

    public List<TabelaPreco> retornaTodos(String pesquisa) {
        List<TabelaPreco> retorno = new ArrayList<>();

        String sql =
                "SELECT " +
                        "id, " +
                        "id_estab, " +
                        "descricao, " +
                        "tipo, " +
                        "perc_padrao," +
                        "permite_desconto, " +
                        "prioriza_promocao, " +
                        "aplica_em " +
                        "FROM " +
                        "tabela_preco " +
                        "WHERE " +
                        "descricao LIKE ? " +
                        "ORDER BY " +
                        "descricao";

        Cursor c = le.rawQuery(sql, new String[]{"%" + pesquisa + "%"});
        c.moveToFirst();

        while (!c.isAfterLast()) {
            TabelaPreco model = new TabelaPreco();

            model.setId(c.getInt(0));
            model.setIdEstab(c.getInt(1));
            model.setDescricao(c.getString(2));
            model.setTipo(c.getString(3));
            model.setPercentualPadrao(c.getDouble(4));
            model.setPermiteDesconto(c.getString(5));
            model.setPriorizaPromocao(c.getString(6));
            model.setAplicaEm(c.getString(7));

            retorno.add(model);

            c.moveToNext();
        }
        c.close();

        return retorno;
    }
}
