package com.higher.construshow.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Departamento implements Serializable {

    private int id;
    private String nome;
    @SerializedName("desconto_maximo")
    private double descontoMaximo;
    @SerializedName("acrescimo_maximo")
    private double acrescimoMaximo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getDescontoMaximo() {
        return descontoMaximo;
    }

    public void setDescontoMaximo(double descontoMaximo) {
        this.descontoMaximo = descontoMaximo;
    }

    public double getAcrescimoMaximo() {
        return acrescimoMaximo;
    }

    public void setAcrescimoMaximo(double acrescimoMaximo) {
        this.acrescimoMaximo = acrescimoMaximo;
    }
}
