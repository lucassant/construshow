package com.higher.construshow.model;

import java.io.Serializable;

public class PedidoLancto implements Serializable {

    private int idEstab;
    private int idPedido;
    private int sequencia;
    private int idProduto;
    private int idEmbalagem;
    private double quantidade;
    private double preco;
    private double descItem;
    private double valorDesconto;
    private double valorDescontoVisualizacao;
    private double precoLiquido;
    private double valorFlexProduto;
    private double porcentagemImposto;
    private double pesoUnidade;
    private double precoVisualizacao;
    private double valorDescontoUnitario;
    private double quantidadeFinal; //é a quantidade que será gravada no construshow (quantidade * peso da unidade)
    private double valorDescontoFinal; //é o valor do desconto que será gravado no construshow (valor desconto * peso da unidade)
    private String nomeProduto;
    private String nomeEmbalagem;
    private double valorTotal;

    public double getValorDescontoUnitario() {
        return valorDescontoUnitario;
    }

    public void setValorDescontoUnitario(double valorDescontoUnitario) {
        this.valorDescontoUnitario = valorDescontoUnitario;
    }

    public double getValorDescontoFinal() {
        return valorDescontoFinal;
    }

    public void setValorDescontoFinal(double valorDescontoFinal) {
        this.valorDescontoFinal = valorDescontoFinal;
    }

    public double getQuantidadeFinal() {
        return quantidadeFinal;
    }

    public void setQuantidadeFinal(double quantidadeFinal) {
        this.quantidadeFinal = quantidadeFinal;
    }

    public double getValorDescontoVisualizacao() {
        return valorDescontoVisualizacao;
    }

    public void setValorDescontoVisualizacao(double valorDescontoVisualizacao) {
        this.valorDescontoVisualizacao = valorDescontoVisualizacao;
    }

    public double getPrecoVisualizacao() {
        return precoVisualizacao;
    }

    public void setPrecoVisualizacao(double precoVisualizacao) {
        this.precoVisualizacao = precoVisualizacao;
    }

    public double getPorcentagemImposto() {
        return porcentagemImposto;
    }

    public void setPorcentagemImposto(double porcentagemImposto) {
        this.porcentagemImposto = porcentagemImposto;
    }

    public double getPesoUnidade() {
        return pesoUnidade;
    }

    public void setPesoUnidade(double pesoUnidade) {
        this.pesoUnidade = pesoUnidade;
    }

    public double getValorFlexProduto() {
        return valorFlexProduto;
    }

    public void setValorFlexProduto(double valorFlexProduto) {
        this.valorFlexProduto = valorFlexProduto;
    }

    public double getPrecoLiquido() {
        return precoLiquido;
    }

    public void setPrecoLiquido(double precoLiquido) {
        this.precoLiquido = precoLiquido;
    }

    public String getNomeEmbalagem() {
        return nomeEmbalagem;
    }

    public void setNomeEmbalagem(String nomeEmbalagem) {
        this.nomeEmbalagem = nomeEmbalagem;
    }

    public double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public int getIdEstab() {
        return idEstab;
    }

    public void setIdEstab(int idEstab) {
        this.idEstab = idEstab;
    }

    public int getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    public int getSequencia() {
        return sequencia;
    }

    public void setSequencia(int sequencia) {
        this.sequencia = sequencia;
    }

    public int getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    public int getIdEmbalagem() {
        return idEmbalagem;
    }

    public void setIdEmbalagem(int idEmbalagem) {
        this.idEmbalagem = idEmbalagem;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public double getDescItem() {
        return descItem;
    }

    public void setDescItem(double descItem) {
        this.descItem = descItem;
    }

    public double getValorDesconto() {
        return valorDesconto;
    }

    public void setValorDesconto(double valorDesconto) {
        this.valorDesconto = valorDesconto;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }
}
