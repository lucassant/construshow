package com.higher.construshow.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Estoque implements Serializable {

    @SerializedName("iditem")
    private int idItem;
    @SerializedName("estab")
    private int idEstab;
    private double saldo;

    public int getIdItem() {
        return idItem;
    }

    public void setIdItem(int idItem) {
        this.idItem = idItem;
    }

    public int getIdEstab() {
        return idEstab;
    }

    public void setIdEstab(int idEstab) {
        this.idEstab = idEstab;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
}
