package com.higher.construshow.view;

import android.content.Intent;
import android.os.Bundle;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.higher.construshow.R;
import com.higher.construshow.adapter.ProdutosAdapter;
import com.higher.construshow.db.ProdutoDao;
import com.higher.construshow.model.Pedido;
import com.higher.construshow.model.Produto;
import com.higher.construshow.model.TabelaPreco;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProdutosActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.btnPesquisar)
    Button btnPesquisar;

    @BindView(R.id.edtPesquisa)
    TextInputEditText edtPesquisa;

    private RecyclerView.LayoutManager mLayoutManager;
    private List<Produto> produtoList;
    private ProdutosAdapter produtosAdapter;

    //Origem 0 = listagem de produtos, origem 1 = tela de pedido
    private int origem = 0;
    //Tipo pedido é o flag que vai listar o departamento dos produtos
    private int tipoPedido = 0;

    private Pedido pedido = null;

    private TabelaPreco tabelaPreco = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produtos);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);

        origem = getIntent().getIntExtra("origem", 0);
        if (origem == 1) {
            getSupportActionBar().setTitle("Selecione o produto");
            pedido = (Pedido) getIntent().getSerializableExtra("pedido");
            tabelaPreco = (TabelaPreco) getIntent().getSerializableExtra("tabela_preco");
            tipoPedido = getIntent().getIntExtra("tipo_pedido", 0);

            if (pedido == null || tabelaPreco == null) {
                Toast.makeText(getApplicationContext(), "Houve um erro ao carregar a lista de produtos, tente novamente", Toast.LENGTH_LONG).show();
                finish();
            }
        }

        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ProdutosActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setVerticalScrollBarEnabled(true);
        recyclerView.setVerticalFadingEdgeEnabled(true);

        carregaLista();

        btnPesquisar.setOnClickListener((view) -> {
            carregaLista();
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void carregaLista() {
        ProdutoDao produtoDao = new ProdutoDao(ProdutosActivity.this);
        produtoList = produtoDao.lista(
                edtPesquisa.getText().toString().isEmpty() ? "" : edtPesquisa.getText().toString().trim(),
                tabelaPreco.getTipo(),
                tabelaPreco.getPercentualPadrao(),
                tipoPedido,
                pedido.getIdEstab()
        );

        produtosAdapter = new ProdutosAdapter(produtoList);
        produtosAdapter.openLoadAnimation();

        produtosAdapter.setOnItemClickListener((adapter, view, position) -> {
            Produto _produto = produtoList.get(position);

            //Verifica se a tela foi aberta pela listagem de produtos normal ou pelo pedido
            if (origem == 1) {
                Intent i = new Intent(ProdutosActivity.this, PedidoLanctoActivity.class);
                i.putExtra("produto", _produto);
                i.putExtra("pedido", pedido);
                i.putExtra("tabela_preco", tabelaPreco);
                i.putExtra("departamento", tipoPedido);
                startActivity(i);
            }
        });

        recyclerView.setAdapter(produtosAdapter);
    }
}
