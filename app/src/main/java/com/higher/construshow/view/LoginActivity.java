package com.higher.construshow.view;

import android.content.Intent;
import android.os.Bundle;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.higher.construshow.R;
import com.higher.construshow.db.VendedorDao;
import com.higher.construshow.model.Vendedor;
import com.higher.construshow.util.Repositorio;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.btnLogin)
    MaterialButton btnLogin;
    @BindView(R.id.edtEmailLogin)
    TextInputEditText edtEmailLogin;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.txvProgress)
    TextView txvProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        btnLogin.setOnClickListener(view -> {
            if (validaCampos()) {
                Toast.makeText(getApplicationContext(), "Digite um email válido", Toast.LENGTH_LONG).show();
            } else {
                progressBar.setVisibility(View.VISIBLE);
                txvProgress.setVisibility(View.VISIBLE);
                btnLogin.setVisibility(View.GONE);

                retornaVendedor();
            }
        });
    }

    private void retornaVendedor() {
        String url = Repositorio.retornaIpConexao() + "login/" + edtEmailLogin.getText().toString().trim();
        Log.i("email", url);

        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, response -> {

            try {
                if (response.getInt("response") == 1) {

                    List<Vendedor> _vendedores = new ArrayList<>();

                    Gson gson = new Gson();

                    _vendedores = gson.fromJson(response.getJSONArray("data").toString(), new TypeToken<List<Vendedor>>() {
                    }.getType());

                    VendedorDao _vendedorDao = new VendedorDao(getApplicationContext());
                    _vendedorDao.insere(_vendedores.get(0));

                    Toast.makeText(LoginActivity.this, "Bem vindo, " + _vendedores.get(0).getNome(), Toast.LENGTH_LONG).show();
                    startActivity(new Intent(LoginActivity.this, EmpresasActivity.class));
                    finish();
                }else{
                    progressBar.setVisibility(View.GONE);
                    txvProgress.setVisibility(View.GONE);
                    btnLogin.setVisibility(View.VISIBLE);

                    Toast.makeText(getApplicationContext(), "Email não encontrado, tente novamente", Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                progressBar.setVisibility(View.GONE);
                txvProgress.setVisibility(View.GONE);
                btnLogin.setVisibility(View.VISIBLE);

                Toast.makeText(getApplicationContext(), "Houve um erro ao conectar, tente novamente", Toast.LENGTH_LONG).show();
            }
        }, error -> {
            progressBar.setVisibility(View.GONE);
            txvProgress.setVisibility(View.GONE);
            btnLogin.setVisibility(View.VISIBLE);

            Toast.makeText(getApplicationContext(), "Houve um erro ao conectar, tente novamente", Toast.LENGTH_LONG).show();
        });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    private boolean validaCampos() {
        String _email = edtEmailLogin.getText().toString().trim();

        return _email.isEmpty();
    }
}
