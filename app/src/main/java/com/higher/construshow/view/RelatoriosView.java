package com.higher.construshow.view;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.MenuItem;
import android.view.View;

import com.higher.construshow.R;
import com.higher.construshow.adapter.RelatoriosAdapter;
import com.higher.construshow.model.Relatorio;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RelatoriosView extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private RelatoriosAdapter adapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relatorios_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);

        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(RelatoriosView.this);
        recyclerView.setLayoutManager(mLayoutManager);

        carregaLista();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int itemId = item.getItemId();
        switch (itemId)
        {
            case android.R.id.home:
                finish();
                break;
        }

        return true;
    }

    private void carregaLista(){
        List<Relatorio> lista = new ArrayList<>();

        Relatorio relatorio = new Relatorio();
        relatorio.setId(1);
        relatorio.setNome("Tabela de preços");
        relatorio.setDescricao("Lista todos os produtos e seus respectivos preços");
        lista.add(relatorio);

        adapter = new RelatoriosAdapter(lista);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));

        adapter.setOnItemClickListener((adapter1, view, position) -> {
            switch (lista.get(position).getId()){
                case 1:
                    startActivity(new Intent(getApplicationContext(), RelatorioProdutosView.class));
                    break;
                default:
                    break;
            }
        });
    }
}
