package com.higher.construshow.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Vibrator;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.higher.construshow.R;
import com.higher.construshow.adapter.PedidoAdapter;
import com.higher.construshow.db.PedidoDao;
import com.higher.construshow.model.Pedido;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PedidosActivity extends AppCompatActivity {

    private final int TIPO_PEDIDO_MATERIAIS = 1;
    private final int TIPO_PEDIDO_FERRAGENS = 2;
    private final int TIPO_PEDIDO_TELHAS = 4;

    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    private RecyclerView.LayoutManager mLayoutManager;

    @BindView(R.id.rbTodos)
    RadioButton rbTodos;
    @BindView(R.id.rbNaoEnviados)
    RadioButton rbNaoEnviados;
    @BindView(R.id.rbEnviados)
    RadioButton rbEnviados;

    @BindView(R.id.edtPesquisa) TextInputEditText edtPesquisa;
    @BindView(R.id.btnPesquisar) Button btnPesquisar;

    @BindView(R.id.btnNovoFerragens)
    ExtendedFloatingActionButton btnNovoFerragens;
    @BindView(R.id.btnNovoMateriais)
    ExtendedFloatingActionButton btnNovoMateriais;
    @BindView(R.id.btnNovoTelhas)
    ExtendedFloatingActionButton btnNovoTelhas;

    private List<Pedido> pedidoList = new ArrayList<Pedido>();
    private PedidoAdapter pedidoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);

        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(PedidosActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);

        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(view -> {
            fab.setExpanded(!fab.isExpanded());
        });

        btnNovoFerragens.setOnClickListener(view -> {
            fab.setExpanded(!fab.isExpanded());
            Intent i = new Intent(getApplicationContext(), PedidoActivity.class);
            i.putExtra("tipo_pedido", TIPO_PEDIDO_FERRAGENS);
            startActivity(i);
        });

        btnNovoMateriais.setOnClickListener(view -> {
            fab.setExpanded(!fab.isExpanded());
            Intent i = new Intent(getApplicationContext(), PedidoActivity.class);
            i.putExtra("tipo_pedido", TIPO_PEDIDO_MATERIAIS);
            startActivity(i);
        });

        btnNovoTelhas.setOnClickListener(view -> {
            fab.setExpanded(!fab.isExpanded());
            Intent i = new Intent(getApplicationContext(), PedidoActivity.class);
            i.putExtra("tipo_pedido", TIPO_PEDIDO_TELHAS);
            startActivity(i);
        });

        btnPesquisar.setOnClickListener(view -> {
            carregaLista();
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        carregaLista();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void carregaLista(){
        PedidoDao pedidoDao = new PedidoDao(PedidosActivity.this);

        String _tipo = "2";
        if(rbTodos.isChecked()){
            _tipo = "2";
        }else if(rbEnviados.isChecked()){
            _tipo = "1";
        }else{
            _tipo = "0";
        }

        pedidoList = pedidoDao.lista(_tipo, edtPesquisa.getText().toString().trim());
        pedidoAdapter = new PedidoAdapter(pedidoList);
        recyclerView.setAdapter(pedidoAdapter);

        pedidoAdapter.setOnItemClickListener((adapter, view, position) -> {
            Pedido _pedido = pedidoList.get(position);

            if(_pedido != null){
                //Verifica se já foi enviado
                if(_pedido.getEnviado() == 0){
                    Intent i = new Intent(getApplicationContext(), PedidoActivity.class);
                    i.putExtra("pedido", _pedido);
                    i.putExtra("tipo_pedido", _pedido.getIdDepartamento());
                    startActivity(i);
                }else{
                    Intent i = new Intent(getApplicationContext(), PedidoEnviadoView.class);
                    i.putExtra("pedido", _pedido);
                    i.putExtra("tipo_pedido", _pedido.getIdDepartamento());
                    startActivity(i);
                }

            }
        });

        pedidoAdapter.setOnItemLongClickListener((adapter, view, position) -> {
            Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(25);

            Pedido _pedido = pedidoList.get(position);
            if(_pedido != null){
                exibeDialog(String.valueOf(_pedido.getId()), String.valueOf(_pedido.getIdEstab()));
            }

            return true;
        });
    }

    private void exibeDialog(String idPedido, String idEstab) {
        AlertDialog.Builder builder = new AlertDialog.Builder(PedidosActivity.this);
        builder.setTitle("Atenção");
        builder.setMessage("Deseja excluir esse pedido?");
        builder.setCancelable(false);
        builder.setPositiveButton("Sim", (dialogInterface, i) -> {
            PedidoDao _pedidoDao = new PedidoDao(getApplicationContext());
            _pedidoDao.excluiPedido(idPedido);
            carregaLista();
            Toast.makeText(getApplicationContext(), "Pedido excluído com sucesso", Toast.LENGTH_LONG).show();
        });

        builder.setNegativeButton("Não", (dialogInterface, i) -> Log.d("cancelar", "clicou"));

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
