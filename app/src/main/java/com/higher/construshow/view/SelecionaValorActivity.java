package com.higher.construshow.view;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.higher.construshow.R;

public class SelecionaValorActivity extends AppCompatActivity {

    private String Valor_tela = "";

    private EditText Numero;

    private Button btnok;
    private Button btnlimpar;
    private Button btnbackspace;

    boolean EscreverDoZero = true;

    int permiteVirgula = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleciona_valor);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Valor_tela = getIntent().getStringExtra("valor_tela");
        permiteVirgula = getIntent().getIntExtra("permite_virgula", 1);

        Numero = findViewById(R.id.edtnumero);

        ButtonCriar(R.id.btn0);
        ButtonCriar(R.id.btn1);
        ButtonCriar(R.id.btn2);
        ButtonCriar(R.id.btn3);
        ButtonCriar(R.id.btn4);
        ButtonCriar(R.id.btn5);
        ButtonCriar(R.id.btn6);
        ButtonCriar(R.id.btn7);
        ButtonCriar(R.id.btn8);
        ButtonCriar(R.id.btn9);

        if(permiteVirgula == 1){
            ButtonCriar(R.id.btnvirgula);
        }

        if (Valor_tela.equals("")) {
            Numero.setText("0");
        } else {
            Numero.setText(Valor_tela.replaceAll("\\.", ""));
        }

        btnlimpar = findViewById(R.id.btnlimpar);
        btnlimpar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Numero.setText("0");
                LabNumeroZero();
                EscreverDoZero = true;
            }
        });

        btnbackspace = findViewById(R.id.btnbackspace);
        btnbackspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((Numero.length() > 0)) {
                    Numero.setText(Numero.getText().toString().substring(0, (Numero.getText().length()) - 1));
                }
            }
        });

        btnok = findViewById(R.id.btnok);
        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Numero.getText().toString().trim().equals("")) {
                    Numero.setText("0");
                }

                if ((Numero.getText().toString().substring((Numero.getText().length()) - 1)).equals(",")) {
                    Numero.setText(Numero.getText().toString().substring(0, (Numero.getText().length()) - 1));
                }

                setResult(RESULT_OK, (new Intent()).setAction(Numero.getText().toString()));
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int itemId = item.getItemId();
        switch (itemId)
        {
            case android.R.id.home:
                finish();
            default:
                break;
        }

        return true;
    }

    public void Incluir(String Num)
    {
        if (EscreverDoZero)
        {
            Numero.setText(Num);
            LabNumeroZero();
            EscreverDoZero = false;
        }
        else if (!(Numero.length() == 11))
        {
            Numero.setText(Numero.getText().toString() + Num);
            LabNumeroZero();
        }
    }

    public String StrPermanece(String Str, String Permanece)
    {
        int I, V;
        String S;
        S = "";
        for (I = 0; I < Str.length(); I++)
        {
            for (V = 0; V < Permanece.length(); V++)
            {
                if (Str.substring(I, I + 1).equals(Permanece.substring(V, V + 1)))
                {
                    S = S + Str.substring(I, I + 1);
                }
            }
        }
        return S;
    }

    public Double MyStrToFloat(String Str)
    {
        String VStr;
        VStr = StrPermanece("0" + Str, ".0123456789-").toString();
        return Double.valueOf(VStr);
    }

    public void LabNumeroZero()
    {
        if ((Numero.length() > 0) && (Numero.getText().toString().substring(0, 1)).equals("0"))
        {
            if (Numero.length() > 1)
            {
                if (Numero.getText().toString().substring(1, 2).equals(",") == false)
                {
                    Numero.setText(Numero.getText().toString().substring(1, Numero.getText().length()));
                }
            }
            else
            {
                Numero.setText(Numero.getText().toString().substring(1, Numero.getText().length()));
            }
        }

        if (MyStrToFloat(Numero.getText().toString()) == 0)
        {
            if ((Numero.getText().toString().equals(",")) || (Numero.getText().toString().equals("0,")))
            {
                Numero.setText("0,");
            }
            else if(Numero.getText().toString().equals("0,0"))
            {
                Numero.setText("0,0");
            }
            else{
                Numero.setText("0");
            }
        }
    }

    public void ButtonCriar(int Nome)
    {
        Button ButTemp;
        ButTemp = (Button) findViewById(Nome);
        ButTemp.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Button ButTemp2;
                ButTemp2 = (Button) v;

                if (ButTemp2.getText().toString().equals(","))
                {
                    if (Numero.getText().toString().indexOf(",") == -1)
                    {
                        Incluir(",");
                    }
                    else
                    {
                        if (EscreverDoZero == true)
                        {
                            Incluir(",");
                        }
                    }
                }
                else
                {
                    Incluir(ButTemp2.getText().toString());
                }
            }
        });
    }

}
