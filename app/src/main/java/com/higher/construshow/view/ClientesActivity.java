package com.higher.construshow.view;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.higher.construshow.R;
import com.higher.construshow.adapter.ClientesAdapter;
import com.higher.construshow.db.ClienteDao;
import com.higher.construshow.model.Cidade;
import com.higher.construshow.model.Cliente;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ClientesActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.btnPesquisar)
    Button btnPesquisar;

    @BindView(R.id.edtPesquisa)
    EditText edtPesquisa;
    @BindView(R.id.edtCidade)
    EditText edtCidade;

    private RecyclerView.LayoutManager mLayoutManager;
    private List<Cliente> listaClientes;

    private ClientesAdapter clientesAdapter;
    //Origem 0 = listagem de clientes, origem 1 = tela de pedido
    private int origem = 0;
    private Cidade cidade = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clientes);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);

        origem = getIntent().getIntExtra("origem", 0);
        if (origem == 1) {
            getSupportActionBar().setTitle("Selecione o cliente");
        }

        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ClientesActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setVerticalScrollBarEnabled(true);
        recyclerView.setVerticalFadingEdgeEnabled(true);

        carregaLista();

        btnPesquisar.setOnClickListener((view) -> {
            carregaLista();
        });

        edtCidade.setOnClickListener(view -> {
            Intent i = new Intent(ClientesActivity.this, CidadesView.class);
            startActivityForResult(i, 1);
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_CANCELED) {
                edtCidade.setText("");
                cidade = null;
            } else {
                cidade = (Cidade) data.getSerializableExtra("cidade");
                if (cidade != null) {
                    edtCidade.setText(cidade.getNome());
                } else {
                    Toast.makeText(getApplicationContext(), "Não foi possível selecionar a cidade, tente novamente", Toast.LENGTH_LONG).show();
                    edtCidade.setText("");
                }
            }
        }
    }

    private void carregaLista() {
        ClienteDao clienteDao = new ClienteDao(ClientesActivity.this);
        listaClientes = clienteDao.retornaTodos(edtPesquisa.getText().toString().trim(), cidade);

        clientesAdapter = new ClientesAdapter(listaClientes);
        clientesAdapter.openLoadAnimation();

        recyclerView.setAdapter(clientesAdapter);

        clientesAdapter.setOnItemClickListener((adapter, view, position) -> {
            Cliente _cliente = listaClientes.get(position);

            //Verifica se a tela foi aberta pela listagem de clientes normal ou pelo pedido
            if (origem == 1) {
                setResult(RESULT_OK, new Intent().putExtra("cliente", _cliente));
                finish();
            } else {
                Intent i = new Intent(ClientesActivity.this, ClienteDetalhesActivity.class);
                i.putExtra("cliente", _cliente);
                startActivity(i);
            }

        });
    }
}
