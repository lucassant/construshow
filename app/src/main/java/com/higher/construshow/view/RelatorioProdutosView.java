package com.higher.construshow.view;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.higher.construshow.R;
import com.higher.construshow.adapter.ProdutosAdapter;
import com.higher.construshow.db.ProdutoDao;
import com.higher.construshow.model.Produto;
import com.higher.construshow.model.TabelaPreco;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RelatorioProdutosView extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.btnPesquisar)
    Button btnPesquisar;

    @BindView(R.id.edtPesquisa)
    EditText edtPesquisa;
    @BindView(R.id.edtTabelaPreco)
    EditText edtTabelaPreco;

    private RecyclerView.LayoutManager mLayoutManager;
    private TabelaPreco tabelaPreco = null;
    private List<Produto> listaProdutos = new ArrayList<>();

    private ProdutosAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relatorio_produtos_view);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);

        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(RelatorioProdutosView.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setVerticalScrollBarEnabled(true);
        recyclerView.setVerticalFadingEdgeEnabled(true);

        carregaLista();

        edtTabelaPreco.setOnClickListener(v -> {
            Intent i = new Intent(RelatorioProdutosView.this, SelecionaTabPrecoView.class);
            startActivityForResult(i, 1);
        });

        btnPesquisar.setOnClickListener(v -> carregaLista());
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_CANCELED) {
                edtTabelaPreco.setText("Clique aqui para selecionar");
                tabelaPreco = null;
            } else {
                tabelaPreco = (TabelaPreco) data.getSerializableExtra("tabela");
                if (tabelaPreco != null) {
                    edtTabelaPreco.setText(tabelaPreco.getDescricao());
                } else {
                    Toast.makeText(getApplicationContext(), "Não foi possível selecionar a tabela de preço, tente novamente", Toast.LENGTH_LONG).show();
                    edtTabelaPreco.setText("Clique aqui para selecionar");
                }
            }
        }
    }

    private void carregaLista(){
        ProdutoDao produtoDao = new ProdutoDao(RelatorioProdutosView.this);

        listaProdutos = produtoDao.listaRelatorioPrecos(edtPesquisa.getText().toString(), tabelaPreco);
        adapter = new ProdutosAdapter(listaProdutos);

        recyclerView.setAdapter(adapter);
    }
}
