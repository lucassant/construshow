package com.higher.construshow.view;

import android.os.Bundle;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.higher.construshow.R;
import com.higher.construshow.db.CidadeDao;
import com.higher.construshow.db.ClienteDao;
import com.higher.construshow.db.ConceitoClienteDao;
import com.higher.construshow.db.EmpresaDao;
import com.higher.construshow.db.EnderecoDao;
import com.higher.construshow.db.EstoqueDao;
import com.higher.construshow.db.ParcelasDao;
import com.higher.construshow.db.ProdutoDao;
import com.higher.construshow.db.TabelaPrecoDao;
import com.higher.construshow.db.VendedorDao;
import com.higher.construshow.model.Cidade;
import com.higher.construshow.model.Cliente;
import com.higher.construshow.model.ConceitoCliente;
import com.higher.construshow.model.Empresa;
import com.higher.construshow.model.Endereco;
import com.higher.construshow.model.Estoque;
import com.higher.construshow.model.Flex;
import com.higher.construshow.model.Parcela;
import com.higher.construshow.model.Produto;
import com.higher.construshow.model.TabelaPreco;
import com.higher.construshow.model.Vendedor;
import com.higher.construshow.util.Repositorio;
import com.higher.construshow.util.Sincronizacao;

import org.json.JSONException;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ImportarActivity extends AppCompatActivity {

    @BindView(R.id.btnImportar)
    Button btnImportar;
    @BindView(R.id.txvSincronizacao)
    TextView txvSincronizacao;
    @BindView(R.id.txvClientes)
    TextView txvClientes;
    @BindView(R.id.txvProdutos)
    TextView txvProdutos;

    private AlertDialog dialogSincronizacao;

    private Empresa empresa = null;
    private Vendedor vendedor = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_importar);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);

        empresa = carregaEmpresa();
        vendedor = carregaVendedor();

        if (empresa != null && vendedor != null) {

            if (!empresa.getUltimaSinc().equals("2010-01-01"))
                txvSincronizacao.setText("Última importação foi feita em " + Repositorio.dataBancoParaTela(empresa.getUltimaSinc()));

        } else {
            Toast.makeText(this, "Houve um erro ao carregar os dados, tente novamente", Toast.LENGTH_LONG).show();
            finish();
        }

        atualizaTotalizadores();

        btnImportar.setOnClickListener(view -> exibeDialog());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void conectar() {
        exibeDialogSincronizar();
        Log.d("iniciou", "agora");
        String url = "http://131.100.251.82:8086/construshow_api/public/api/importar/500/" + empresa.getUltimaSinc() + "/" + vendedor.getIdPessoa();

        RequestQueue requestQueue = Volley.newRequestQueue(ImportarActivity.this);

        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, response -> {
            Gson gson = new Gson();

            try {
                //Cidades
                List<Cidade> cidadeList = gson.fromJson(response.getJSONArray("cidades").toString(), new TypeToken<List<Cidade>>() {
                }.getType());
                long _inserts = 0;
                CidadeDao cidadeDao = new CidadeDao(ImportarActivity.this);
                for (Cidade cidade : cidadeList) {
                    cidadeDao.insere(cidade);
                    _inserts++;
                }
                Log.d("finalizou cidades", "-> " + _inserts);
                _inserts = 0;

                //Clientes
                List<Cliente> clienteList = gson.fromJson(response.getJSONArray("clientes").toString(), new TypeToken<List<Cliente>>() {
                }.getType());
                ClienteDao clienteDao = new ClienteDao(ImportarActivity.this);
                for (int i = 0; i < clienteList.size(); i++) {
                    clienteDao.insertOuUpdate(clienteList.get(i));
                    _inserts++;
                }
                Log.d("finalizou clientes", "-> " + _inserts);
                _inserts = 0;

                //Endereços
                List<Endereco> enderecoList = gson.fromJson(response.getJSONArray("enderecos").toString(), new TypeToken<List<Endereco>>() {
                }.getType());
                EnderecoDao enderecoDao = new EnderecoDao(ImportarActivity.this);
                for (int i = 0; i < enderecoList.size(); i++) {
                    enderecoDao.insertOuUpdate(enderecoList.get(i));
                    _inserts++;
                }
                Log.d("finalizou endereços", "-> " + _inserts);
                _inserts = 0;

                //Produtos
                List<Produto> produtos = gson.fromJson(response.getJSONArray("produtos").toString(), new TypeToken<List<Produto>>() {
                }.getType());

                ProdutoDao produtoDao = new ProdutoDao(ImportarActivity.this);
                for (int i = 0; i < produtos.size(); i++) {
                    produtoDao.insertOuUpdate(produtos.get(i));
                    _inserts++;
                }
                Log.d("finalizou produtos", "-> " + _inserts);
                _inserts = 0;

                //Estoques
                List<Estoque> estoques = gson.fromJson(response.getJSONArray("estoques").toString(), new TypeToken<List<Estoque>>() {
                }.getType());
                EstoqueDao estoqueDao = new EstoqueDao(ImportarActivity.this);
                for (Estoque es : estoques) {
                    estoqueDao.insertOuUpdate(es);
                    _inserts++;
                }
                Log.d("finalizou estoques", "-> " + _inserts);
                _inserts = 0;

                //Tabelas de preço
                List<ConceitoCliente> conceitos = gson.fromJson(response.getJSONArray("conceito_cliente").toString(), new TypeToken<List<ConceitoCliente>>() {
                }.getType());

                ConceitoClienteDao conceitoClienteDao = new ConceitoClienteDao(getApplicationContext());
                for (ConceitoCliente cc : conceitos) {
                    conceitoClienteDao.insere(cc);
                    _inserts++;
                }
                Log.d("finalizou conceitos", "-> " + _inserts);
                _inserts = 0;
                //Tabelas de preço
                List<TabelaPreco> tabelas = gson.fromJson(response.getJSONArray("tabelas").toString(), new TypeToken<List<TabelaPreco>>() {
                }.getType());

                TabelaPrecoDao tabelaPrecoDao = new TabelaPrecoDao(getApplicationContext());
                for (TabelaPreco tab : tabelas) {
                    tabelaPrecoDao.insertOuUpdate(tab);
                    _inserts++;
                }
                Log.d("finalizou tabelas preço", "-> " + _inserts);
                _inserts = 0;

                //Parcelamentos
                List<Parcela> parcelas = gson.fromJson(response.getJSONArray("parcelas").toString(), new TypeToken<List<Parcela>>() {
                }.getType());
                ParcelasDao parcelasDao = new ParcelasDao(getApplicationContext());
                for (Parcela parcela : parcelas) {
                    parcelasDao.insertOuUpdate(parcela);
                    _inserts++;
                }
                Log.d("finalizou parcelamentos", "-> " + _inserts);

                // Atualiza o saldo flex
                List<Flex> flex = gson.fromJson(response.getJSONArray("flex").toString(), new TypeToken<List<Flex>>(){
                }.getType());
                atualizaSaldoFlex(flex);

                //Atualiza a data da ultima sincronização
                empresa.setUltimaSinc(Repositorio.formataDataParaSQL(Repositorio.retornaDataAtual()));
                Log.d("nova sinc", empresa.getUltimaSinc());
                atualizaUltimaSinc();
                dialogSincronizacao.dismiss();

                atualizaTotalizadores();

                //Sincronizacao da retaguarda do grupo itamar
                Sincronizacao sincronizacao = new Sincronizacao(ImportarActivity.this);
                sincronizacao.sincronizaProdutos();

            } catch (JSONException e) {
                Toast.makeText(ImportarActivity.this, "Json error -> " + e.toString(), Toast.LENGTH_LONG).show();
                Log.d("json error", e.toString());
                dialogSincronizacao.dismiss();
            }
        }, error -> {
            Log.d("finalizou", error.toString());
            Toast.makeText(ImportarActivity.this, "connection error -> " + error.toString(), Toast.LENGTH_LONG).show();
            dialogSincronizacao.dismiss();
        });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    private void exibeDialogSincronizar() {
        LayoutInflater layoutInflater = LayoutInflater.from(ImportarActivity.this);

        View view = layoutInflater.inflate(R.layout.dialog_importacao, null);
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(ImportarActivity.this);

        alertBuilder.setView(view);
        alertBuilder.setCancelable(false);

        //Cria o dialog
        dialogSincronizacao = alertBuilder.create();
        dialogSincronizacao.show();
    }

    private void exibeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ImportarActivity.this);
        builder.setTitle("Atenção");
        builder.setMessage("Deseja iniciar a importação de dados?");
        builder.setCancelable(false);
        builder.setPositiveButton("Sim", (dialogInterface, i) -> conectar());

        builder.setNegativeButton("Não", (dialogInterface, i) -> Log.d("cancelar", "clicou"));

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private Empresa carregaEmpresa() {
        EmpresaDao empresaDao = new EmpresaDao(ImportarActivity.this);
        return empresaDao.carrega();
    }

    private Vendedor carregaVendedor() {
        VendedorDao vendedorDao = new VendedorDao(ImportarActivity.this);
        return vendedorDao.retorna();
    }

    private void atualizaUltimaSinc() {
        EmpresaDao empresaDao = new EmpresaDao(ImportarActivity.this);
        empresaDao.atualiza(empresa);

        txvSincronizacao.setText("Última importação foi feita em " + Repositorio.dataBancoParaTela(empresa.getUltimaSinc()));
        Toast.makeText(ImportarActivity.this, "Importação finalizada com sucesso!", Toast.LENGTH_LONG).show();
    }

    private void atualizaTotalizadores() {
        ClienteDao _clienteDao = new ClienteDao(ImportarActivity.this);
        txvClientes.setText(String.valueOf(_clienteDao.qtdClientes()));

        ProdutoDao _produtoDao = new ProdutoDao(ImportarActivity.this);
        txvProdutos.setText(String.valueOf(_produtoDao.qtdProdutos()));
    }

    private void atualizaSaldoFlex(List<Flex> lista){
        VendedorDao vendedorDao = new VendedorDao(getApplicationContext());
        for(Flex f : lista){
            vendedorDao.atualizaFlex(f.getSaldoAtual());
            Log.d("novo saldo", "-> " + f.getSaldoAtual());
        }
    }
}
