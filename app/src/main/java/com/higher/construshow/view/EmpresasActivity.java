package com.higher.construshow.view;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import com.higher.construshow.R;
import com.higher.construshow.adapter.EmpresasAdapter;
import com.higher.construshow.db.EmpresaDao;
import com.higher.construshow.model.Empresa;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EmpresasActivity extends AppCompatActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private EmpresasAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empresas);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);

        carregaLista();
    }

    private void carregaLista() {
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(EmpresasActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);

        List<Empresa> lista = new ArrayList<>();

        lista.add(new Empresa(500, "Itamar Ferragens"));

        adapter = new EmpresasAdapter(lista);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));

        adapter.setOnItemClickListener((adapter1, view, position) -> {
            //Obtem a empresa selecionada
            Empresa _empresa = lista.get(position);

            if (_empresa != null) {

                addEmpresa(_empresa);

                Intent i = new Intent(EmpresasActivity.this, PrincipalActivity.class);
                i.putExtra("empresa", _empresa);
                startActivity(i);
            }

        });
    }

    private void addEmpresa(Empresa empresa) {
        EmpresaDao empresaDao = new EmpresaDao(this);
        empresaDao.salvar(empresa);
    }
}
