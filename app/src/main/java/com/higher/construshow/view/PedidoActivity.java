package com.higher.construshow.view;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Vibrator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import com.higher.construshow.MainActivity;
import com.higher.construshow.R;
import com.higher.construshow.adapter.PedidoLanctoAdapter;
import com.higher.construshow.db.ClienteDao;
import com.higher.construshow.db.EnderecoDao;
import com.higher.construshow.db.ParcelasDao;
import com.higher.construshow.db.PedidoDao;
import com.higher.construshow.db.PedidoLanctoDao;
import com.higher.construshow.db.TabelaPrecoDao;
import com.higher.construshow.db.VendedorDao;
import com.higher.construshow.model.Cliente;
import com.higher.construshow.model.Endereco;
import com.higher.construshow.model.Parcela;
import com.higher.construshow.model.Pedido;
import com.higher.construshow.model.PedidoLancto;
import com.higher.construshow.model.TabelaPreco;
import com.higher.construshow.model.Vendedor;
import com.higher.construshow.util.GeraPdf;
import com.higher.construshow.util.Repositorio;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PedidoActivity extends AppCompatActivity {

    @BindView(R.id.textViewEmpresa)
    TextView textViewEmpresa;
    @BindView(R.id.spinnerEmpresas)
    Spinner spinnerEmpresas;

    @BindView(R.id.linearCliente)
    LinearLayout linearCliente;
    @BindView(R.id.txvCliente)
    TextView txvCliente;
    @BindView(R.id.imageView)
    ImageView imgCliente;

    @BindView(R.id.linearTabelaPreco)
    LinearLayout linearTabelaPreco;
    @BindView(R.id.txvTabPreco)
    TextView txvTabPreco;
    @BindView(R.id.txvPercTabPreco)
    TextView txvPercTabPreco;

    @BindView(R.id.linearParcelas)
    LinearLayout linearParcelas;
    @BindView(R.id.txvParcelas)
    TextView txvParcelas;

    @BindView(R.id.card_endereco)
    MaterialCardView cardEndereco;
    @BindView(R.id.spinner)
    Spinner spinner;

    @BindView(R.id.spinnerFormas)
    Spinner spinnerFormas;

    //Campos do card endereço
    @BindView(R.id.linearTopoCard)
    LinearLayout linearTopoCard;
    @BindView(R.id.txvCnpj)
    TextView txvCnpj;
    @BindView(R.id.txvTipoEndereco)
    TextView txvTipoEndereco;
    @BindView(R.id.txvNomeFantasia)
    TextView txvNomeFantasia;
    @BindView(R.id.txvEndereco)
    TextView txvEndereco;
    @BindView(R.id.txvCidade)
    TextView txvCidade;

    @BindView(R.id.btnAddProduto)
    Button btnAddProduto;
    @BindView(R.id.btnVerItens)
    Button btnVerItens;
    @BindView(R.id.txvDataPedido)
    TextView txvDataPedido;
    @BindView(R.id.txvQtdItens)
    TextView txvQtdItens;
    @BindView(R.id.txvFlexPedido)
    TextView txvFlexPedido;
    @BindView(R.id.edtObservacao)
    EditText edtObservacao;
    @BindView(R.id.btnEnviarPdf)
    Button btnEnviarPdf;
    @BindView(R.id.btnEnviarPedido)
    Button btnEnviarPedido;
    @BindView(R.id.btnDuplicarPedido)
    MaterialButton btnDuplicarPedido;

    //Bottomsheet
    @BindView(R.id.bottom_sheet)
    LinearLayout bottomSheet;
    @BindView(R.id.txvValorTotal)
    TextView txvValorTotal;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private RecyclerView.LayoutManager mLayoutManager;

    private BottomSheetBehavior sheetBehavior;

    private Pedido pedido = null;
    private Cliente cliente = null;
    private Endereco endereco = null;
    private TabelaPreco tabelaPreco = null;
    private Parcela parcela = null;
    private Vendedor vendedor = null;

    private List<PedidoLancto> lanctos = new ArrayList<>();

    public static int numPedido = 0;

    private int tipoPedido = 0;

    private AlertDialog dialogSincronizacao;

    private boolean e_alteracao = false;

    private final List<String> formas = Arrays.asList("Boleto/Duplicata", "Cartão de crédito", "Cheque");
    //matriz = 500, lapão = 1002
    private final List<String> empresas = Arrays.asList("Matriz", "Lapão");

    private String formaPagSelecionada = "Cartão de crédito";
    private String empresaSelecionada = "Matriz";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedido);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);

        sheetBehavior = BottomSheetBehavior.from(bottomSheet);

        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayout.VERTICAL));
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        pedido = (Pedido) getIntent().getSerializableExtra("pedido");
        tipoPedido = getIntent().getIntExtra("tipo_pedido", 0);

        vendedor = carregaVendedor();

        habilitaArmazenamento();

        if (vendedor == null) {
            Toast.makeText(getApplicationContext(), "Houve um erro ao carregar os dados, tente novamente", Toast.LENGTH_LONG).show();
            finish();
        }

        if (pedido != null) {

            e_alteracao = true;

            formaPagSelecionada = pedido.getFormaPagamento();

            //Obtem a empresa do pedido
            switch (pedido.getIdEstab()) {
                case 500:
                    empresaSelecionada = "Matriz";
                    break;
                case 1001:
                    empresaSelecionada = "Matriz";
                    break;
                case 1002:
                    empresaSelecionada = "Lapão";
                    break;
            }

            //Muda o título da barra
            getSupportActionBar().setTitle(pedido.getIdDepartamento() == 1 ? "Pedido - Materiais" : "Pedido - Ferragens");

            if (pedido.getIdDepartamento() == 1) {
                escondeSpinnerEmpresa();
            }

            //Carrega o cliente
            ClienteDao _clienteDao = new ClienteDao(getApplicationContext());
            cliente = _clienteDao.retorna(pedido.getIdCliente());

            //Carrega a tabela de preço
            TabelaPrecoDao _tabelaPrecoDao = new TabelaPrecoDao(getApplicationContext());
            tabelaPreco = _tabelaPrecoDao.retorna(pedido.getIdTabelaPreco());

            //Carrega o parcelamento
            ParcelasDao _parcelasDao = new ParcelasDao(getApplicationContext());
            parcela = _parcelasDao.retorna(pedido.getIdParcelamento());

            //Carrega o endereço
            EnderecoDao _enderecoDao = new EnderecoDao(getApplicationContext());
            endereco = _enderecoDao.retorna(String.valueOf(pedido.getIdEndereco()));

            txvCliente.setText(cliente.getNome());
            txvDataPedido.setText(Repositorio.dataBancoParaTela(pedido.getData()));
            txvFlexPedido.setText("R$ " + Repositorio.formataNumero(pedido.getValorFlex()));

            txvTabPreco.setText(tabelaPreco.getDescricao());
            txvPercTabPreco.setText((tabelaPreco.getTipo().equals("D") ? "- " : "+ ") + Repositorio.formataNumero(tabelaPreco.getPercentualPadrao()) + "%");
            txvPercTabPreco.setVisibility(View.VISIBLE);

            txvParcelas.setText(parcela.getDescricao());

            edtObservacao.setText(pedido.getObservacao());
            imgCliente.setVisibility(View.GONE);
            linearTopoCard.setVisibility(View.GONE);

        } else {
            e_alteracao = false;

            txvDataPedido.setText(Repositorio.dataParaTela(Repositorio.retornaDataAtual()));
            btnEnviarPdf.setVisibility(View.GONE);
            btnDuplicarPedido.setVisibility(View.GONE);
            btnEnviarPedido.setVisibility(View.GONE);

            switch (tipoPedido) {
                case 0:
                    Toast.makeText(getApplicationContext(), "Houve um erro ao carregar o tipo do pedido, tente novamente", Toast.LENGTH_LONG).show();
                    finish();
                    break;
                case 1:
                    getSupportActionBar().setTitle("Pedido - Materiais");
                    escondeSpinnerEmpresa();
                    break;
                case 2:
                    getSupportActionBar().setTitle("Pedido - Ferragens");
                    break;
                default:
                    break;
            }
        }

        carregaSpinnerEmpresas();
        carregaSpinnerFormasPagamento();

        dadosCard();

        linearCliente.setOnClickListener(view -> {
            if (!e_alteracao) {
                Intent i = new Intent(PedidoActivity.this, ClientesActivity.class);
                i.putExtra("origem", 1);
                startActivityForResult(i, 1);
            }

        });

        linearTabelaPreco.setOnClickListener(view -> {

            Intent i = new Intent(PedidoActivity.this, SelecionaTabPrecoView.class);
            startActivityForResult(i, 2);

        });

        linearParcelas.setOnClickListener(view -> {

            Intent i = new Intent(PedidoActivity.this, SelecionaParcelamentoView.class);
            i.putExtra("cliente", cliente);
            i.putExtra("forma_pagamento", formaPagSelecionada);
            startActivityForResult(i, 3);

        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                endereco = (Endereco) adapterView.getSelectedItem();
                dadosCard();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                endereco = null;
                dadosCard();
            }
        });

        spinnerFormas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                //Verifica se selecionou uma forma de pagamento diferente
                if (!adapterView.getSelectedItem().toString().equals(formaPagSelecionada)) {
                    formaPagSelecionada = (String) adapterView.getSelectedItem();
                    parcela = null;
                    txvParcelas.setText("Selecione o parcelamento");
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                formaPagSelecionada = "Cartão de crédito";
            }
        });

        spinnerEmpresas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                //Verifica se selecionou uma forma de pagamento diferente
                if (!adapterView.getSelectedItem().toString().equals(empresaSelecionada)) {
                    empresaSelecionada = (String) adapterView.getSelectedItem();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnAddProduto.setOnClickListener(view ->
                gravar(1)
        );

        btnVerItens.setOnClickListener(view -> {
            if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else {
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });

        btnEnviarPedido.setOnClickListener(view -> exibeDialog());

        btnEnviarPdf.setOnClickListener(view -> {
            geraPdf();
        });

        btnDuplicarPedido.setOnClickListener(v -> {
            Repositorio.exibeDialogDuplicar(PedidoActivity.this, String.valueOf(pedido.getId()));
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        carregaLista();

        if (pedido != null) {
            btnEnviarPdf.setVisibility(View.VISIBLE);
            btnDuplicarPedido.setVisibility(View.VISIBLE);
            btnEnviarPedido.setVisibility(View.VISIBLE);

            atualizaComponentesTela();
        }
    }

    private void atualizaComponentesTela() {
        int _qtdItens = 0;
        double _total = 0.00;

        PedidoDao _pedidoDao = new PedidoDao(getApplicationContext());
        _qtdItens = _pedidoDao.qtdTotalProdutos(String.valueOf(pedido.getId()));
        _total = _pedidoDao.valorTotalPedido(String.valueOf(pedido.getId()));

        if (tabelaPreco.getTipo().equals("A")) {
            _total += _total * (tabelaPreco.getPercentualPadrao() / 100);
        } else {
            _total -= _total * (tabelaPreco.getPercentualPadrao() / 100);
        }

        txvQtdItens.setText(_qtdItens + " produtos");
        txvValorTotal.setText("R$ " + Repositorio.formataNumero(_total));
        txvFlexPedido.setText("R$ " + Repositorio.formataNumero(
                _pedidoDao.retornaTotalFlex(
                        String.valueOf(pedido.getId()))
        ));
    }

    private void geraPdf() {
        GeraPdf geraPdf = new GeraPdf(PedidoActivity.this);

        geraPdf.openDocument(String.valueOf(pedido.getId()));

        //Adiciona linhas iniciais do pdf
        geraPdf.addMetaData(PrincipalActivity.empresa.getNome(), "Orçamento", "Construshow");
        geraPdf.addTitles(PrincipalActivity.empresa.getNome(), "Orçamento", Repositorio.dataParaTela(Repositorio.retornaDataAtual()));

        geraPdf.addVendedor(vendedor.getNome());

        geraPdf.addDadosCliente(cliente.getNome(), cliente.getEndereço(), cliente.getCidade() + " - " + cliente.getUf());

        //Cria os headers da table
        String[] header = {"Produto", "Unidade", "Qtd", "Preço", "Subtotal"};

        ArrayList<String[]> rows = new ArrayList<>();

        for (int i = 0; i < lanctos.size(); i++) {

            //Calcula o valor unitário do desconto
            double auxDescontoUnitario = 0.00;
            auxDescontoUnitario = lanctos.get(i).getValorDesconto() / lanctos.get(i).getQuantidade();

            rows.add(new String[]{
                    lanctos.get(i).getIdProduto() + " - " + lanctos.get(i).getNomeProduto(),
                    lanctos.get(i).getNomeEmbalagem(),
                    Repositorio.formataNumero(lanctos.get(i).getQuantidade()),

                    Repositorio.formataNumero((lanctos.get(i).getPreco() - auxDescontoUnitario)),
                    Repositorio.formataNumero((lanctos.get(i).getPreco() * lanctos.get(i).getQuantidade()) - lanctos.get(i).getValorDesconto())});
        }

        geraPdf.createTable(header, rows);

        PedidoDao orcamentoDao = new PedidoDao(getApplicationContext());
        double auxTotal = 0.00;
        int auxQtdItens = 0;
        auxTotal = orcamentoDao.valorTotalPedido(String.valueOf(pedido.getId()));
        auxQtdItens = orcamentoDao.qtdTotalProdutos(String.valueOf(pedido.getId()));

        //Adiciona os totalizadores
        geraPdf.addTotalizadores(
                auxQtdItens,
                Repositorio.dataBancoParaTela(pedido.getData()),
                Repositorio.formataNumero(auxTotal)
        );

        geraPdf.closeDocument();

        geraPdf.pdfWhatsapp(PedidoActivity.this);
    }

    private void conectaPedido() {
        String url = "http://131.100.251.82:8086/construshow_api/public/api/pedido";

        RequestQueue requestQueue = Volley.newRequestQueue(PedidoActivity.this);
        Gson gson = new Gson();

        try {
            String _jsonPedido = Repositorio.jsonPedido(getApplicationContext(), String.valueOf(pedido.getId()));

            //Retorna os lançamentos
            PedidoLanctoDao pedidoLanctoDao = new PedidoLanctoDao(getApplicationContext());
            List<PedidoLancto> _pedidos = pedidoLanctoDao.lista(String.valueOf(pedido.getId()));
            String _jsonLanctos = gson.toJson(_pedidos);

            JSONObject jsonBody = new JSONObject();
            jsonBody.put("pedido", _jsonPedido);
            jsonBody.put("vendedor", 1);
            jsonBody.put("lanctos", _jsonLanctos);

            Log.d("pedido", _jsonPedido);
            Log.d("lanctos", _jsonLanctos);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonBody, response -> {
                Log.d("response", response.toString());
                try {
                    //Se a resposta for -1 então é repetido, se for 1 cadastrou com sucesso, se for 0 houve erro ao inserir
                    int _resposta = response.getInt("response");

                    if (_resposta == 1 || _resposta == -1) {
                        pedido.setEnviado(1);
                        pedido.setIdCarrinho(response.getInt("numero_controle"));

                        int _qtdInseridos = response.getInt("quantidade");

                        PedidoDao _pedDao = new PedidoDao(getApplicationContext());
                        long _qtd = _pedDao.atualizaParaEnviado(pedido);

                        //Se qtd > 0 então foi atualizado com sucesso
                        if (_qtd > 0) {
                            Toast.makeText(PedidoActivity.this, "Pedido com " + _qtdInseridos + " itens enviado com sucesso!", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(PedidoActivity.this, "Erro ao atualizar o pedido", Toast.LENGTH_LONG).show();
                        }
                    }

                    dialogSincronizacao.dismiss();
                    finish();
                } catch (JSONException e) {
                    dialogSincronizacao.dismiss();
                    Toast.makeText(getApplicationContext(), "Não foi possível enviar o pedido, tente novamente", Toast.LENGTH_LONG).show();
                }

            }, error -> {
            });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjectRequest);

        } catch (JSONException e) {
            Log.d("json exception", e.toString());
        }
    }

    private void exibeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(PedidoActivity.this);
        builder.setTitle("Atenção");
        builder.setMessage("Deseja enviar esse pedido? \n\nDepois de enviado não será possível alterar.");
        builder.setCancelable(false);
        builder.setPositiveButton("Enviar", (dialogInterface, i) -> {
            exportaPedido();
        });

        builder.setNegativeButton("Cancelar", (dialogInterface, i) -> Log.d("cancelar", "clicou"));

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void exportaPedido() {
        exibeDialogSincronizar();
        conectaPedido();
    }

    private void exibeDialogSincronizar() {
        LayoutInflater layoutInflater = LayoutInflater.from(PedidoActivity.this);

        View view = layoutInflater.inflate(R.layout.dialog_sincronizar, null);
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(PedidoActivity.this);

        alertBuilder.setView(view);
        alertBuilder.setCancelable(false);

        //Cria o dialog
        dialogSincronizacao = alertBuilder.create();
        dialogSincronizacao.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.pedido_capa, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.gravar:
                gravar(0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_CANCELED) {
                txvCliente.setText("Selecione o cliente");
                cliente = null;
                dadosCard();
            } else {
                cliente = (Cliente) data.getSerializableExtra("cliente");
                if (cliente != null) {
                    txvCliente.setText(cliente.getNome());
                    dadosCard();
                    carregaSpinner();
                } else {
                    Toast.makeText(getApplicationContext(), "Não foi possível selecionar o cliente, tente novamente", Toast.LENGTH_LONG).show();
                    txvCliente.setText("Selecione o cliente");
                    dadosCard();
                }
            }
        } else if (requestCode == 2) {
            if (resultCode == RESULT_CANCELED) {
                txvTabPreco.setText("Selecione a tabela de preço");
                tabelaPreco = null;
                txvPercTabPreco.setVisibility(View.GONE);
            } else {
                tabelaPreco = (TabelaPreco) data.getSerializableExtra("tabela");
                if (tabelaPreco != null) {
                    txvTabPreco.setText(tabelaPreco.getDescricao());
                    txvPercTabPreco.setVisibility(View.VISIBLE);
                    txvPercTabPreco.setText((tabelaPreco.getTipo().equals("D") ? "- " : "+ ") + Repositorio.formataNumero(tabelaPreco.getPercentualPadrao()) + "%");
                } else {
                    Toast.makeText(getApplicationContext(), "Não foi possível selecionar a tabela de preço, tente novamente", Toast.LENGTH_LONG).show();
                    txvTabPreco.setText("Selecione a tabela de preço");
                    txvPercTabPreco.setVisibility(View.GONE);
                }
            }
        } else if (requestCode == 3) {
            if (resultCode == RESULT_CANCELED) {
                txvParcelas.setText("Selecione o parcelamento");
                parcela = null;
            } else {
                parcela = (Parcela) data.getSerializableExtra("parcelamento");
                if (parcela != null) {
                    txvParcelas.setText(parcela.getDescricao());
                } else {
                    Toast.makeText(getApplicationContext(), "Não foi possível selecionar o parcelamento, tente novamente", Toast.LENGTH_LONG).show();
                    txvParcelas.setText("Selecione o parcelamento");
                }
            }
        }
    }

    private void gravar(int origem) {
        if (validaCampos().equals("")) {

            PedidoDao pedidoDao = new PedidoDao(getApplicationContext());

            if (pedido == null) {
                pedido = new Pedido();

                //Obtem o id que será salvo
                pedido.setId(pedidoDao.nextNumPedido());
            }

            pedido.setIdVendedor(vendedor.getId());

            //Se for ferragens ele obtem a empresa selecionada, se for materiais é empresa 1001

            int _idEmpresa = 500;

            if (tipoPedido == 2) {
                if (!empresaSelecionada.equals("Matriz")) {
                    _idEmpresa = 1002;
                }
            } else {
                _idEmpresa = 1001;
            }

            pedido.setIdEstab(_idEmpresa);

            pedido.setIdCliente(Integer.valueOf(cliente.getId()));

            pedido.setFormaPagamento(formaPagSelecionada);
            pedido.setIdTabelaPreco(tabelaPreco.getId());
            pedido.setIdParcelamento(parcela.getId());

            pedido.setIdEndereco(Integer.valueOf(endereco.getIdEndereco()));
            pedido.setData(Repositorio.dataTelaParaBanco(txvDataPedido.getText().toString()));
            pedido.setObservacao(edtObservacao.getText().toString());
            pedido.setIdDepartamento(tipoPedido);

            long _id = pedidoDao.save(pedido);

            //Se o _id > 0 então ele deu certo
            if (_id > 0) {
                pedido.setId((int) _id);

                Toast.makeText(getApplicationContext(), "Pedido " + _id + " salvo com sucesso", Toast.LENGTH_LONG).show();

                /**
                 * origem = 1 : clicou em adicionar produto
                 * origem = 0 : clicou no botão de salvar
                 */
                if (origem == 1) {
                    numPedido = pedido.getId();

                    Intent j = new Intent(PedidoActivity.this, ProdutosActivity.class);
                    j.putExtra("origem", 1);
                    j.putExtra("pedido", pedido);
                    j.putExtra("tabela_preco", tabelaPreco);
                    j.putExtra("tipo_pedido", tipoPedido);
                    startActivity(j);
                } else {
                    finish();
                }

            } else {
                Toast.makeText(getApplicationContext(), "Não foi possível salvar o pedido, tente novamente", Toast.LENGTH_LONG).show();
            }

        } else {
            Toast.makeText(getApplicationContext(), validaCampos(), Toast.LENGTH_LONG).show();
        }
    }

    private String validaCampos() {
        if (cliente == null && endereco == null) {
            return "Selecione um cliente antes de gravar";
        } else if (tabelaPreco == null) {
            return "Selecione uma tabela de preço";
        } else if (parcela == null) {
            return "Selecione um parcelamento";
        } else {
            return "";
        }
    }

    private void dadosCard() {

        if (cliente != null) {
            cardEndereco.setVisibility(View.VISIBLE);

            txvCnpj.setText(Repositorio.formataCnpjCpf(cliente.getCnpjf()));
            txvNomeFantasia.setText(cliente.getFantasia());

            if (endereco != null) {
                String _end = endereco.getEndereco();
                _end = _end + (endereco.getNumero() == null || endereco.getNumero().equals("") ? "" : ", " + endereco.getNumero());
                _end = _end + (endereco.getBairro() == null ? "" : " - " + endereco.getBairro());

                txvTipoEndereco.setText(endereco.getTipo().equals("P") ? "Principal" : "Cobrança");

                txvEndereco.setText(_end);
                txvCidade.setText(endereco.getCidade() + " - " + endereco.getUf());
            } else {
                txvTipoEndereco.setText("");
                txvEndereco.setText("");
                txvCidade.setText("");
            }
        } else {
            cardEndereco.setVisibility(View.GONE);
        }
    }

    private void carregaLista() {
        if (pedido != null) {
            PedidoLanctoDao _pedidoLanctoDao = new PedidoLanctoDao(getApplicationContext());

            lanctos = _pedidoLanctoDao.lista(String.valueOf(pedido.getId()));
            PedidoLanctoAdapter pedidoLanctoAdapter = new PedidoLanctoAdapter(lanctos);
            recyclerView.setAdapter(pedidoLanctoAdapter);

            pedidoLanctoAdapter.setOnItemClickListener((adapter, view, position) -> {
                Intent i = new Intent(PedidoActivity.this, PedidoLanctoActivity.class);
                i.putExtra("pedido", pedido);
                i.putExtra("lancto", lanctos.get(position));
                i.putExtra("departamento", tipoPedido);
                startActivity(i);
            });

            pedidoLanctoAdapter.setOnItemLongClickListener((adapter, view, position) -> {
                Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibrator.vibrate(50);

                PedidoLancto _pedidoLancto = lanctos.get(position);
                if (_pedidoLancto != null) {
                    exibeDialogExcluirLancto(_pedidoLancto);
                }

                return false;
            });

        }
    }

    private void carregaSpinner() {
        EnderecoDao enderecoDao = new EnderecoDao(getApplicationContext());

        List<Endereco> enderecoList = enderecoDao.lista(cliente.getId());
        ArrayAdapter spinnerAdapter = new ArrayAdapter(PedidoActivity.this, android.R.layout.simple_spinner_dropdown_item, enderecoList);
        spinner.setAdapter(spinnerAdapter);
    }

    private void carregaSpinnerFormasPagamento() {
        ArrayAdapter spinnerAdapter = new ArrayAdapter(PedidoActivity.this, android.R.layout.simple_spinner_dropdown_item, formas);
        spinnerFormas.setAdapter(spinnerAdapter);

        if (e_alteracao) {
            spinnerFormas.setSelection(spinnerAdapter.getPosition(formaPagSelecionada));
        }
    }

    private void carregaSpinnerEmpresas() {
        ArrayAdapter spinnerAdapter = new ArrayAdapter(PedidoActivity.this, android.R.layout.simple_spinner_dropdown_item, empresas);
        spinnerEmpresas.setAdapter(spinnerAdapter);

        if (e_alteracao) {
            spinnerEmpresas.setSelection(spinnerAdapter.getPosition(empresaSelecionada));
        }
    }

    private Vendedor carregaVendedor() {
        VendedorDao vendedorDao = new VendedorDao(getApplicationContext());
        return vendedorDao.retorna();
    }


    private void exibeDialogExcluirLancto(final PedidoLancto _lancto) {
        AlertDialog.Builder builder = new AlertDialog.Builder(PedidoActivity.this);

        builder.setTitle("Atenção");
        builder.setMessage("Deseja excluir esse produto?");
        builder.setCancelable(false);

        builder.setPositiveButton("Sim", (dialogInterface, i) -> {

            PedidoLanctoDao _pedidoLanctoDao = new PedidoLanctoDao(PedidoActivity.this);

            _pedidoLanctoDao.excluiLancto(
                    String.valueOf(pedido.getId()),
                    String.valueOf(_lancto.getSequencia()),
                    _lancto.getValorFlexProduto());

            Toast.makeText(PedidoActivity.this, "Produto excluído com sucesso", Toast.LENGTH_SHORT).show();

            carregaLista();
            atualizaComponentesTela();
        });

        builder.setNegativeButton("Não", (dialogInterface, i) -> Log.d("cancelar", "clicou"));

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void habilitaArmazenamento() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

        } else {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 9999);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 9999:

                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    Toast.makeText(getApplicationContext(), "Para continuar é necessário aceitar.", Toast.LENGTH_LONG).show();
                    finish();
                }

                break;
        }
    }

    private void escondeSpinnerEmpresa() {
        textViewEmpresa.setVisibility(View.GONE);
        spinnerEmpresas.setVisibility(View.GONE);
    }
}
