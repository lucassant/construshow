package com.higher.construshow.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.higher.construshow.R;
import com.higher.construshow.model.Empresa;

import java.util.List;

public class EmpresasAdapter extends BaseQuickAdapter<Empresa, BaseViewHolder> {

    public EmpresasAdapter(@Nullable List<Empresa> data) {
        super(R.layout.card_empresa, data);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, Empresa item) {
        helper.setText(R.id.txvEmpresa, item.getId() + " - " + item.getNome());
    }
}
