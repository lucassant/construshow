package com.higher.construshow.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.higher.construshow.R;
import com.higher.construshow.model.Cidade;

import java.util.List;

public class CidadesAdapter extends BaseQuickAdapter<Cidade, BaseViewHolder> {

    public CidadesAdapter(@Nullable List<Cidade> data) {
        super(R.layout.card_cidade, data);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, Cidade item) {
        helper.setText(R.id.txvTitulo, item.getNome());
    }
}
