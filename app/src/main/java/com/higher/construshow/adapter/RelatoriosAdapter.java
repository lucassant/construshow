package com.higher.construshow.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.higher.construshow.R;
import com.higher.construshow.model.Relatorio;

import java.util.List;

public class RelatoriosAdapter extends BaseQuickAdapter<Relatorio, BaseViewHolder> {
    public RelatoriosAdapter(@Nullable List<Relatorio> data) {
        super(R.layout.card_relatorio, data);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, Relatorio item) {
        helper.setText(R.id.txvRelatorio, item.getNome())
                .setText(R.id.txvDescricao, item.getDescricao());
    }
}
