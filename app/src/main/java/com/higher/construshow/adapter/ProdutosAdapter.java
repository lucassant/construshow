package com.higher.construshow.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.higher.construshow.R;
import com.higher.construshow.model.Produto;
import com.higher.construshow.util.Repositorio;

import java.util.List;

public class ProdutosAdapter extends BaseQuickAdapter<Produto, BaseViewHolder> {

    public ProdutosAdapter(@Nullable List<Produto> data) {
        super(R.layout.card_produto, data);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, Produto item) {
        helper.setText(R.id.txvProduto, item.getDescricao())
                .setText(R.id.txvCodProduto, item.getId())
                .setText(R.id.txvUnidade, item.getUnidade())
                .setText(R.id.txvMarca, item.getMarca() != null ? item.getMarca() : "")
                .setText(R.id.txvPesoUnidade, Repositorio.formataNumero(item.getPesoUnidade()))
                .setText(R.id.txvEstoque, Repositorio.formataNumero(item.getEstoque()))
                .setText(R.id.txvPreco, "R$ " + Repositorio.formataNumero(item.getPrecoLiquido()));
    }
}
