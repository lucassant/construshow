package com.higher.construshow.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.higher.construshow.R;
import com.higher.construshow.model.Cliente;
import com.higher.construshow.util.Repositorio;

import java.util.List;

public class ClientesAdapter extends BaseQuickAdapter<Cliente, BaseViewHolder> {

    public ClientesAdapter(@Nullable List<Cliente> data) {
        super(R.layout.card_clientes, data);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, Cliente item) {
        helper.setText(R.id.txvTitulo, item.getNome())
                .setText(R.id.txvCnpj, Repositorio.formataCnpjCpf(item.getCnpjf()))
                .setText(R.id.txvNomeFantasia, item.getFantasia())
                .setText(R.id.txvEndereco, item.getEndereço())
                .setText(R.id.txvCidade, item.getCidade());
    }
}
