package com.higher.construshow.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.higher.construshow.R;
import com.higher.construshow.model.Endereco;
import com.higher.construshow.util.Repositorio;

import java.util.List;

public class EnderecoAdapter extends BaseQuickAdapter<Endereco, BaseViewHolder> {

    public EnderecoAdapter(@Nullable List<Endereco> data) {
        super(R.layout.card_enderecos, data);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, Endereco item) {
        helper.setText(R.id.txvTipo, item.getTipo().equals("P") ? "Principal" : "Cobrança")
                .setText(R.id.txvEndereco, item.getEndereco() + ", " + item.getNumero())
                .setText(R.id.txvBairro, item.getBairro())
                .setText(R.id.txvCidade, item.getCidade() + " - " + item.getUf())
                .setText(R.id.txvCep, Repositorio.formataCep(item.getCep()))
                .setText(R.id.txvTelefone, item.getTelefone());
    }
}
