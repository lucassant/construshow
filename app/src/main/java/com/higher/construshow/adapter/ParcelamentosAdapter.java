package com.higher.construshow.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.higher.construshow.R;
import com.higher.construshow.model.Parcela;

import java.util.List;

public class ParcelamentosAdapter extends BaseQuickAdapter<Parcela, BaseViewHolder> {

    public ParcelamentosAdapter(@Nullable List<Parcela> data) {
        super(R.layout.card_parcelamentos, data);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, Parcela item) {
        helper.setText(R.id.txvTitulo, item.getDescricao())
                .setText(R.id.txvNroParcelas, item.getNumeroParcelas() > 1 ? item.getNumeroParcelas() + " parcelas" : item.getNumeroParcelas() + " parcela");
    }
}
