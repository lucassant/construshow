package com.higher.construshow.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.higher.construshow.R;
import com.higher.construshow.model.TabelaPreco;
import com.higher.construshow.util.Repositorio;

import java.util.List;

public class TabPrecoAdapter extends BaseQuickAdapter<TabelaPreco, BaseViewHolder> {

    public TabPrecoAdapter(@Nullable List<TabelaPreco> data) {
        super(R.layout.card_tabela_preco, data);
    }

    @Override
    protected void convert(@NonNull BaseViewHolder helper, TabelaPreco item) {
        helper.setText(R.id.txvTitulo, item.getDescricao())
                .setText(R.id.txvTipo, item.getTipo().equals("A") ? "Acréscimo" : "Desconto")
                .setText(R.id.txvPercentual, Repositorio.formataNumero(item.getPercentualPadrao()) + "%")
                .setText(R.id.txvPermiteDesconto, item.getPermiteDesconto().equals("S") ? "Sim" : "Não");
    }
}
